'use strict'
/**
 * Dependencies
 * @ignore
 */
const cwd = process.cwd()
const path = require('path')
const express = require('express')
const morgan = require('morgan')
/**
 * Dotenv
 * @ignore
 */
require('dotenv').config()
/**
 * Express
 * @ignore
 */
const {
    PORT: port = 3000
} = process.env
const app = express()
app.use(express.static('public/build'))
// GET /message
app.get('/message', (req, res) => {
    res.json({
        message: 'Hello, Greg!',
    })
})
/**
 * Launch app
 * @ignore
 */
app.listen(port, () => console.log(`Listening on port ${port}`))