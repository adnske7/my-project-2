import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/index.scss'

/**
 * Global Constants
 */
var posts = [];
var users = [];
/**
 * Variables
 */
var titles = [];

/**
 * Wait for the DOM to be ready
 * jQuery needs to cache to DOM :)
 */
$(document).ready(function () {
    // TODO Add your listeners here

    $.getJSON("https://jsonplaceholder.typicode.com/posts/", function (data) {
        data.forEach(element => {
            posts.push(new post(element.id, element.userId, element.title, element.body));
        });
    }).done(function (data) {

        $.getJSON("https://jsonplaceholder.typicode.com/users", function (data) {
            data.forEach(element => {
                users.push(new user(element.id, element.name, element.username, element.email));
            });

        }).done(function (data) {

            showBlogList();
        })


    });



})

class post {
    constructor(id, userId, title, body) {
        this.id = id;
        this.userId = userId;
        this.title = title;
        this.body = body;
    }
    getUserId() {
        return this.userId;
    }
    getTitle() {
        return this.title;
    }
    getBody() {
        return this.body;
    }
    getId() {
        return this.id;
    }
}

class user {
    constructor(id, name, username, email) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
    }
    getId() {
        return this.id;
    }
    getName() {
        return this.name;
    }
    getUsername() {
        return this.username;
    }
    getEmail() {
        return this.email;
    }
}

function getBlogPost(blogId) {
    return posts.find(element => element.getId() === blogId);
}

function getUser(userId) {
    return users.find(element => element.getId() === userId);
}

function showBlogPost(blogId) {
    const post = getBlogPost(blogId);
    const user = getUser(post.getUserId());
    const root = $("#app");
    //empty the container
    root.empty();

    //Display post title
    root.append(`
    <h2 style="width: 75%;">${post.getTitle()}</h2><br>`);

    // Display body
    root.append(`
    <p style="margin: 2em; padding-right: 20%">${post.getBody()}</p>`);

    // Display user information
    root.append(`
    <div id = "author" style = "padding-top: 3em;">
        <br><br>
        <p>author:</p>
        <h3>${user.getName()} (${user.getUsername()})</h3>
        <p>${user.getEmail()}</p>
    </div>`)

    //Back button
    root.append(`<button type="button" id="return"><- Return</button>`);

    $("#return").click(function () {
        showBlogList();
    });

    // move stuff around
    $("author").css("float", "left");
    $("#return").css("float", "right");
    $("#return").css("margin-left", "50%");
    $("#return").css("height", "2em");

    //style button
    $("#return").css("background-color", "#808080");
    $("#return").css("color", "white");
    $("#return").css("border", "none");
}

function showBlogList() {
    const root = $("#app");
    root.empty();

    // ul start
    root.append(`<ul id="list" class="list-group"></ul>`);

    let list = $("#list"); 

    //for each alle post

    posts.forEach(element => {

        let title = element.getTitle();
        // li start

        list.append(`<li class="list-group-item" onclick>${title}</li>`);

        $("li").last().click(function () {
            showBlogPost(element.getId())
        })

        $("li").last().css("cursor", "pointer");

        //root.append(`</ul>`);

    });
}